<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Level extends Model
{
    use HasFactory;

    public function users(){
        return $this->hasMany(User::class);
    }

    public function posts(){
        return $this->hasManyThrought(Post::class, User::class);
    }

    public function videos(){
        return $this->hasManyThrought(Video::class, User::class);
    }
}
